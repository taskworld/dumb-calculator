//
//  TWViewController.m
//  Dumb Calculator
//
//  Created by Sikhapol Saijit on 2/20/14.
//  Copyright (c) 2014 Taskworld. All rights reserved.
//

#import "TWViewController.h"

#import "TWCalculator.h"

@interface TWViewController ()

@property (strong, nonatomic) TWCalculator *calculator;

@end

@implementation TWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.calculator = [[TWCalculator alloc] init];
}

- (IBAction)numberPadDidTap:(UIButton *)sender
{
    self.resultLabel.text = [NSString stringWithFormat:@"%ld", (long)sender.tag];
    [self.calculator addOperand:sender.tag];
}

- (IBAction)operatorPadDidTap:(UIButton *)sender
{
    [self.calculator addOperator:sender.tag];
}

- (IBAction)resultButtonDidTap:(id)sender
{
    NSInteger result = [self.calculator performCalculation];
    self.resultLabel.text = [NSString stringWithFormat:@"%ld", result];
}

- (IBAction)allClearButtonDidTap:(id)sender
{
    self.resultLabel.text = @"0";
    [self.calculator allClear];
}

@end
