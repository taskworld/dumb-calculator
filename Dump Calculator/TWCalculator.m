//
//  TWCalculator.m
//  Dumb Calculator
//
//  Created by Sikhapol Saijit on 2/21/14.
//  Copyright (c) 2014 Taskworld. All rights reserved.
//

#import "TWCalculator.h"

@interface TWCalculator ()

@property (strong, nonatomic) NSMutableArray *operands;
@property (strong, nonatomic) NSMutableArray *operators;

@end

@implementation TWCalculator

- (id)init
{
    self = [super init];
    if (self) {
        self.lastResult = 0;
        self.operands = [[NSMutableArray alloc] init];
        self.operators = [[NSMutableArray alloc] init];
        [self.operators addObject:@(TWCalculatorOperatorTypePlus)];
    }
    return self;
}

- (void)addOperand:(NSInteger)operand
{
    [self.operands addObject:@(operand)];
}

- (void)addOperator:(TWCalculatorOperatorType)type
{
    [self.operators addObject:@(type)];
}

- (NSInteger)performCalculation
{
    while ([self.operands count] && [self.operators count]) {
        TWCalculatorOperatorType operator = [[self.operators firstObject] integerValue];
        [self.operators removeObjectAtIndex:0];
        NSInteger operand = [[self.operands firstObject] integerValue];
        [self.operands removeObjectAtIndex:0];
        switch (operator) {
            case TWCalculatorOperatorTypePlus:
                self.lastResult += operand;
                break;
            case TWCalculatorOperatorTypeMinus:
                self.lastResult -= operand;
                break;
            default:
                break;
        }
    }
    [self.operators removeAllObjects];
    [self.operands removeAllObjects];
    return self.lastResult;
}

- (void)allClear
{
    self.lastResult = 0;
    [self.operands removeAllObjects];
    [self.operators removeAllObjects];
}

@end
