//
//  main.m
//  Dump Calculator
//
//  Created by Sikhapol Saijit on 2/21/14.
//  Copyright (c) 2014 Taskworld. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TWAppDelegate class]));
    }
}
