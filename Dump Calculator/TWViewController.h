//
//  TWViewController.h
//  Dumb Calculator
//
//  Created by Sikhapol Saijit on 2/20/14.
//  Copyright (c) 2014 Taskworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TWViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

@end
