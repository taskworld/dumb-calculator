//
//  TWAppDelegate.h
//  Dump Calculator
//
//  Created by Sikhapol Saijit on 2/21/14.
//  Copyright (c) 2014 Taskworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
