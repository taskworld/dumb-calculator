//
//  TWCalculator.h
//  Dumb Calculator
//
//  Created by Sikhapol Saijit on 2/21/14.
//  Copyright (c) 2014 Taskworld. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, TWCalculatorOperatorType) {
    TWCalculatorOperatorTypePlus     = 0,
    TWCalculatorOperatorTypeMinus    = 1,
    TWCalculatorOperatorTypeMultiply = 2,
    TWCalculatorOperatorTypeDivide   = 3
};

@interface TWCalculator : NSObject

@property (assign, nonatomic) NSInteger lastResult;

- (void)addOperand:(NSInteger)operand;
- (void)addOperator:(TWCalculatorOperatorType)type;

- (NSInteger)performCalculation;
- (void)allClear;

@end
